﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX19
{
    public partial class Form2 : Form
    {
        string[] lines;
        public Form2(string user)
        {
            lines = System.IO.File.ReadAllLines((user+"BD.txt"));
            InitializeComponent();
        }

        private void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
        {
            for(int i = 0;i<lines.Length;i++)
            {
                if(monthCalendar1.SelectionStart.ToString().Substring(0, monthCalendar1.SelectionStart.ToString().IndexOf(' ')) == lines[i].Substring(lines[i].IndexOf(',') + 1))
                {
                    lbl.Text = "ביום זה יש ל" + lines[i].Substring(0, lines[i].IndexOf(',')) + " יום הולדת";
                }
            }
        }

        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Close();
        }
    }
}
