﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX19
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void loginBtn_Click(object sender, EventArgs e)
        {
            string[] lines = System.IO.File.ReadAllLines("Users.txt");
            string a = "";
            for(int i = 0;i<lines.Length;i++)
            {
                a = lines[i];
                if (a.Substring(a.IndexOf(',') + 1) == txtPass.Text && a.Substring(0, a.IndexOf(',')) == txtUser.Text)
                {
                    this.Hide();
                    Form2 f2 = new Form2(a.Substring(0, a.IndexOf(',')));
                    f2.Show();
                }
            }
        }
    }
}
